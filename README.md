# John Conway's game of life

A simple interpretation of the game of life using the C++ language.

# Use

	git clone <repository>
	cd folder
	make
	./CPPLife
