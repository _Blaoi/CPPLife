CC=g++
CFLAGS=-W -Wall -Werror -g -O0 
LFLAGS=
S=src
O=bin
H=inc

all:directories CPPLife

directories:
	mkdir -p $(O)

CPPLife:$(O)/main.o
	$(CC) -o $@ $(O)/main.o $(CFLAGS) $(LFLAGS)

$(O)/main.o: $(S)/main.cpp $(H)/main.hpp
	$(CC) -c $(S)/main.cpp -o $@ $(CFLAGS) $(LFLAGS)

clean:
	rm -rf $(O)
	rm -rf $(S)/*.swp
	rm -rf $(S)/.*.swp
	rm -rf $(H)/*.swp
	rm -rf $(H)/.*.swp

mrproper: clean
	rm -rf CPPLife

.PHONY:directories clean mrproper
