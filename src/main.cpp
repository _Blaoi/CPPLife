#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "../inc/main.hpp"

using namespace std;

const char CELL_EMPTY = '.';
const char CELL_ALIVE = '*';
static int WIDTH;
static int HEIGHT;
static int GENERATION;

int main(){
	float time_interval;
	GENERATION = 0;
	cout << "Welcome in the game of life" << endl;
	cout << "Which width ?" << endl;
	do{
		cin >> WIDTH;
		if(WIDTH < 5 || WIDTH > 100){
			cout << "Width must be in the [5;100[ range.\n";
		}
	}while(WIDTH < 5 || WIDTH > 100);
	cout << "Which height ?" << endl;
	do{
		cin >> HEIGHT;
		if(HEIGHT < 5 || HEIGHT > 100){
			cout << "Height must be in [5;100[ range.\n";
		}
	}while(HEIGHT < 5 || HEIGHT > 100);
	cout << "Time interval (sec)?" << endl;
	cin >> time_interval;
	char choice;
	do{
		cout << RED << "R" << RESET << "andom generation or " << RED << "D" << RESET << "efined by yourself ?" << endl;
		cout << "(R/D): ";
		cin >> choice;
	}while(choice != 'R' && choice != 'D');
	char** world;
	if(choice == 'R'){
		int probability;
		do{
			cout << "Probability ? ]1;100]\n";
			cin >> probability;
		}while(probability < 1 && probability > 100); 
		world = createRandomWorld(probability);
	}else{
		world = createEmptyWorld();
		do{
			cout << "Press q/Q to quit\n";
		}while(getchar() != 27);
	}
	while(true){
		printWorld(world);
		usleep(time_interval * 1000000);
		++GENERATION;
		world = nextGeneration(world);
	}
	return 0; 
}

char** nextGeneration(char** world){
	char** tmp = copyWorld(world);
	for(int i = 1;i < HEIGHT - 1;++i){
		for(int j = 1;j < WIDTH - 1;++j){
			int neighbours = countNeighbours(tmp,i,j);
			if(tmp[i][j] == CELL_EMPTY){
				if(neighbours == 3){
					tmp[i][j] = CELL_ALIVE;
				}
			}else{
				if(neighbours != 2 && neighbours != 3){
					tmp[i][j] = CELL_EMPTY;
				}
			}
		}
	}
	return tmp;
}

char** copyWorld(char** world){
	char** tmp = createEmptyWorld();
	for(int i = 0;i < HEIGHT;++i){
		for(int j = 0;j < WIDTH;++j){
			tmp[i][j] = world[i][j];
		}
	}
	return tmp;
}
int countNeighbours(char** world,int x, int y){
	int sum = 0;
	if(world[x+1][y] == CELL_ALIVE){
		++sum;
	}
	if(world[x-1][y] == CELL_ALIVE){
		++sum;
	}
	if(world[x][y+1] == CELL_ALIVE){
		++sum;
	}
	if(world[x][y-1] == CELL_ALIVE){
		++sum;
	}
	if(world[x-1][y-1] == CELL_ALIVE){
		++sum;
	}
	if(world[x+1][y+1] == CELL_ALIVE){
		++sum;
	}
	if(world[x+1][y-1] == CELL_ALIVE){
		++sum;
	}
	if(world[x-1][y+1] == CELL_ALIVE){
		++sum;
	}
	return sum;
}
char** createEmptyWorld(){
	char** tmp = new char*[HEIGHT];
	for(int i = 0;i < HEIGHT;++i){
		tmp[i] = new char[WIDTH];
	}
	for(int i = 0;i < HEIGHT;++i){
		for(int j = 0;j < WIDTH;++j){
			tmp[i][j] = CELL_EMPTY;
		}
	}
	return tmp;
}

char** createRandomWorld(int probability){
	char** tmp = new char*[HEIGHT];
	for(int i = 0;i < HEIGHT;++i){
		tmp[i] = new char[WIDTH];
	}
		for(int i = 0;i < HEIGHT;++i){
			for(int j = 0;j < WIDTH;++j){
				if(((rand() % 100) + 1) <= probability){
					tmp[i][j] = CELL_ALIVE;
				}else{
					tmp[i][j] = CELL_EMPTY;
				}
			}
		}
	
	return tmp;
}
void printWorld(char** world){
	cout << "\033[2J\033[1;1H";
	cout << GREEN <<"Generation n°" << RESET << RED <<  GENERATION << RESET << endl;
	for(int i = 0;i < HEIGHT;i++){
		if(i == 0){
			cout << BOLDRED << "+ ";
			for(int j = 0;j < WIDTH;j++){
				cout << "-";
			}
			cout << " +\n";
		}
		cout << "| " << RESET;
		for(int j = 0;j < WIDTH;j++){
			if(world[i][j] == CELL_ALIVE){
				cout << BOLDGREEN << world[i][j] << RESET;
			}else{
				cout << world[i][j];
			}
		}
		cout << BOLDRED << " |\n";
		if(i == HEIGHT - 1){
			cout << "+ ";
			for(int j = 0;j < WIDTH;++j){
				cout << "-";
			}
			cout << " +\n" << RESET;
		}
	}
}
